This is a simple blog template (and web server) with CW support! It lets you hide what you're writing about behind a content warning if you need, and works both with and without JavaScript.

### Dependencies

- Node.js

That's it! No, you don't even need NPM.

### Installation

Just copy the server somewhere, and put your blog posts and style.css in a `content/` folder, organized however you like (style.css needs to be at the root).

### Running the server

Just do `cd <wherever you put the server>; node server.js <port you want>`.

### Author & License

This was written by Ylfingr. It's public domain! See [COPYING.md](COPYING.md) for more information.
