//
// Handle CWs client-side if possible
//
// Written by Ylfingr on 2021-07-23
//

let cwContent = undefined

function setupCWButton() {
	console.log("JS available, setting up CW button.")
	let button = document.querySelector(".cw-button")
	if (button == null) {
		console.log("No CW to set up.")
		return; // don't bother setting up the button if it doesn't exist
	}
	button.href = "javascript:void(0);"
	button.onclick = openCW
	fetchCWContent()
}
async function fetchCWContent() {
	if (cwContent) {
		return cwContent
	} else {
		let response = await fetch("?cw-content-only=1")
		cwContent = await response.text()
		console.log("CW content fetched.")
		return cwContent
	}
}

async function openCW() {
	let cwbody = document.querySelector("cw-body")
	let text = await fetchCWContent()
	cwbody.innerHTML = text
	
	let button = document.querySelector(".cw-button")
	button.onclick = closeCW
	button.textContent = "Show Less"
}

async function closeCW() {
	let cwbody = document.querySelector("cw-body")
	cwbody.innerHTML = ""
	
	let button = document.querySelector(".cw-button")
	button.onclick = openCW
	button.textContent = "Show More"
}

setupCWButton()
