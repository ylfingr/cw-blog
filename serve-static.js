// Serve anything in static/.

let path = require("path")
let fs = require("fs").promises

async function serveStatic(dir, request, response) {
	try {
		let data = await getContent(dir, request)
		response.writeHead(200)
		response.end(data)
	} catch (err) {
		console.log(err)
		response.writeHead((err.code === "ENOENT") ? 404 : 500)
		response.end(`${err.message}\n`)
	}
}

async function getContent(dir, request) {
	let url = new URL(request.url, `http://${request.headers.host}`)
	let pathname = path.normalize(url.pathname)
	// console.log(pathname, `[${request.url}]`)
	
	let diskpath = path.join(dir, pathname)
	// console.log("Path on disk:", diskpath)
	
	let stats = await fs.stat(diskpath)
	if (stats.isDirectory()) {
		diskpath = path.join(diskpath, "index.html")
	}
	let data = await fs.readFile(diskpath)
	return data
}

module.exports = { serveStatic, getContent }
