//
// a very simple CW-enabled blog post server
// CWed content goes in <cw-body> elements; use ?showmore=1 to show the stuff. You should have an <a> with class "showmore" as a show/hide link; when the content is shown, the link text gets changed to "Show Less" and any ?showmore=1 gets removed from the end.
//
// Written by Ylfingr on 2021-07-23.
//

let http = require("http")
let serveStatic = require("./serve-static.js")
let path = require("path")
let fs = require("fs").promises

let port = process.argv[2]
if (!port) {
	console.error("Please give a port as an argument!")
	process.exit(1)
}

async function handleRequest(request, response) {
	let url = new URL(request.url, `http://${request.headers.host}`)
	let showmore = await url.searchParams.get("showmore")
	let cwContentOnly = await url.searchParams.get("cw-content-only")
	// if (showmore) {
		// await serveStatic.serveStatic("content", request, response)
	// } else {
	try {
		let content = await serveStatic.getContent("content", request)
		if (String(content).startsWith("<!DOCTYPE html>")) {
			// template it!
			content = await templateReplace(String(content))
			
			if (cwContentOnly) {
				content = content.replace(/^[\s\S]*<cw-body>\n([\s\S]*\n).*<\/cw-body>[\s\S]*$/, "$1")
				// console.log(content)
			} else {
				if (showmore) {
					content = content.replace(/(<a .*?)\?showmore=1(.*?>)Show More(<\/a>)/, "$1$2Show Less$3")
				} else {
					content = content.replace(/<cw-body>[\s\S]*?<\/cw-body>/, "<cw-body></cw-body>")
				}
			}
			// console.log(content)
		}
		response.end(content)
	} catch (err) {
		console.log(err)
		response.writeHead((err.code === "ENOENT") ? 404 : 500)
		response.end(`${err.message}\n`)
	}
}

async function templateReplace(content) {
	let templateRegex = /{{(.*)}}/g
	let match = undefined;
	while ((match = templateRegex.exec(content)) != null) {
		// console.log(match);
		let filename = path.normalize(match[1])
		let templateContents = String(await fs.readFile(path.join("template/", filename)))
		// trim it to not mess up indent
		templateContents = templateContents.trim()
		// console.log("Template contents:", templateContents)
		
		let index = match.index
		let length = match[0].length
		content = content.substring(0, index) + templateContents + content.substring(index + length)
	}
	return content;
}

let server = http.createServer(handleRequest)
server.listen(port, () => {
	console.log("Listening on port", port)
})
